$(".owl-carousel").owlCarousel({
  loop: true,
  margin: 25,
  center: true,
  nav: true,
  dots: false,
  navText: ["<img src='img/Leftbutton.svg' alt='tombol-kiri'/>", "<img src='img/Rightbutton.svg' alt='tombol-kanan'/>"],
  autoWidth: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 2,
    },
  },
});

